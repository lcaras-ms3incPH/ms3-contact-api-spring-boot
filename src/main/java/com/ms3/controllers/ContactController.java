package com.ms3.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ms3.models.Identification;
import com.ms3.services.ContactService;

@RestController
public class ContactController {

	@Autowired
	private ContactService contactService;
	
	@RequestMapping("/contacts")
	public List<Identification> getContacts() {
		return contactService.getAllContacts();
	}
}
