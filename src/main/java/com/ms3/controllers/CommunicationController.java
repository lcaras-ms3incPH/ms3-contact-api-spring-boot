package com.ms3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ms3.services.CommunicationService;

@RestController
public class CommunicationController {

	@Autowired
	private CommunicationService communicationService;
	
	@RequestMapping("/contacts/{contactId}/communications/")
	public String getCommunicationByContactId(@PathVariable String contactId) {
		return communicationService.getCommunicationsByContactId(Integer.parseInt(contactId));
	}
}
