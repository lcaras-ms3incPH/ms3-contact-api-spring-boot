package com.ms3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms3ContactsApiSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ms3ContactsApiSpringBootApplication.class, args);
	}

}

